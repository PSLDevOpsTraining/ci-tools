# Agular testing image
FROM avatsaev/angular-chrome-headless
# Create build folder for app
RUN mkdir /app
# Copy repo contents to app folder
COPY . /app
# Set working directory to run commands
WORKDIR /app
# Install firebase and dependencies
RUN npm install -g firebase-tools && npm install
# Set working directory to firebase funtions
WORKDIR /app/functions
# Install firebase functions dependencies
RUN npm install
# Return to project main folder
WORKDIR /app
